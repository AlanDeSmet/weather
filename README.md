[Madison, Wisconsin Weather](https://alandesmet.gitlab.io/weather/)


Useful Resources
================

- [Official API](https://www.weather.gov/documentation/services-web-api#/).
   Includes ability to run simple tests directly summary with experimentation

- [More formally defined API](https://github.com/weather-gov/api/blob/master/assets/openapi.yaml)

- [Information on daily weather icons](https://api.weather.gov/icons). These are referred to in the full forecast data.

- [NEXRAD official data sources](https://www.ncei.noaa.gov/products/radar/next-generation-weather-radar).
  These can provide base reflectivity (basically, rainclouds). You probably
  want "base digital reflectivity", which is processed. We can also get useful
  stuff like 1-hour rainfall total. On the down side, I haven't figured out how
  to actually get the data!

- Tempest
	- [Central midwest states](https://tempest.aos.wisc.edu/radar/chase3comphtml5.html)
	- [Colorado and adjacent states](https://tempest.aos.wisc.edu/radar/co3comphtml5.html)
	- [Wisconsin and some of neighbors](https://tempest.aos.wisc.edu/radar/d1_wi3comphtml5.html)
	- [Southern Wisconsin](https://tempest.aos.wisc.edu/radar/d3_swi3comphtml5.html)
	- [Mid-Atlantic states](https://tempest.aos.wisc.edu/radar/ma3comphtml5.html)
	- [Northern midwest states](https://tempest.aos.wisc.edu/radar/mw3comphtml5.html)
	- [Northeastern states](https://tempest.aos.wisc.edu/radar/ne3comphtml5.html)
	- [Pacific north-west states](https://tempest.aos.wisc.edu/radar/nw3comphtml5.html)
	- [Peurto Rico](https://tempest.aos.wisc.edu/radar/pr3comphtml5.html)
	- [Southeastern states](https://tempest.aos.wisc.edu/radar/se3comphtml5.html)
	- [South-central states (Texas centered)](https://tempest.aos.wisc.edu/radar/sp3comphtml5.html)
	- [Southwestern states](https://tempest.aos.wisc.edu/radar/sw3comphtml5.html)
	- [Contiguous 48 states](https://tempest.aos.wisc.edu/radar/us3comphtml5.html)
    - There are also precipitation storm total graphs (look for `*Pcomphtml5.html`), but they appear to only ever add data.

- Maps? https://www.weather.gov/gis/cloudgiswebservices
- Maps? https://www.weather.gov/gis/WebServices
