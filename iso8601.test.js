"use strict";

import test from "ava";


import { date_cmp, date_plus_s, duration_to_approximate_s, baredatestr_to_date, tz_to_s, datestr_to_date, datestr_range_to_dates } from "./public/iso8601.js";

const MINUTE_S = 60;
const HOUR_S = 60*MINUTE_S;
const HOUR_MS = HOUR_S*1000;

test("duration_to_approximate_s", t=> {
	t.is(duration_to_approximate_s("P1D"), 24*HOUR_S);
	t.is(duration_to_approximate_s("PT1H"),  1*HOUR_S);
	t.is(duration_to_approximate_s("PT3H"),  3*HOUR_S);
});

test("baredatestr_to_date", t=>{
	t.is(baredatestr_to_date("2022-01-02T12:34:56").getTime(), 1641148496*1000);
	t.deepEqual(baredatestr_to_date("2022-01-02T12:34:56"), new Date("2022-01-02T12:34:56"));
});

test("tz_to_s", t=>{
	t.is(tz_to_s("-06:00"), -6*HOUR_S);
	t.is(tz_to_s("+00:00"),  0*HOUR_S);
	t.is(tz_to_s("+03:30"),  3*HOUR_S + 30*MINUTE_S);
});

test("datestr_to_date", t=>{
	t.is(datestr_to_date("2022-01-02T12:34:56").getTime(), 1641148496*1000);
	t.is(datestr_to_date("2022-01-02T12:34:56-06:00").getTime(), 1641148496*1000+6*HOUR_MS);
});

test("datestr_range_to_dates", t=>{
	var x = datestr_range_to_dates("2022-01-02T12:34:56-03:00/PT3H");
	t.is(x[0].getTime(), 1641148496*1000+3*HOUR_MS);
	t.is(x[1].getTime(), 1641148496*1000+3*HOUR_MS + 3*HOUR_MS);
	t.deepEqual(datestr_range_to_dates("2022-01-02T12:34:56-03:00/PT3H"),
		[new Date("2022-01-02T15:34:56"), new Date("2022-01-02T18:34:56")]);
});

test("date_plus_s", t=>{
	t.deepEqual(date_plus_s(new Date("2022-01-02T12:34:56"),  3600), new Date("2022-01-02T13:34:56"));
	t.deepEqual(date_plus_s(new Date("2022-01-02T12:34:56"), -3600), new Date("2022-01-02T11:34:56"));
	t.deepEqual(date_plus_s(new Date("2022-01-02T12:34:56"),     0), new Date("2022-01-02T12:34:56"));
});

function sign_only(x) {
	if(x > 0) { return 1; }
	if(x < 0) { return -1; }
	return 0;
}

test("date_cmp", t=>{
	var medium = new Date("2022-01-02T12:34:56");
	var small1 = new Date("2022-01-02T12:34:55");
	var small2 = new Date("2000-01-02T12:34:56");
	var big1 = new Date("2022-01-02T12:34:57");
	var big2 = new Date("2032-01-02T12:34:56");
	var medium2 = new Date("2022-01-02T12:34:56");
	t.is(          date_cmp(medium, medium2),  0);
	t.is(sign_only(date_cmp(medium, small1)),  1);
	t.is(sign_only(date_cmp(medium, small2)),  1);
	t.is(sign_only(date_cmp(medium, big1)),   -1);
	t.is(sign_only(date_cmp(medium, big2)),   -1);

	t.is(sign_only(date_cmp(small1, medium)), -1);
	t.is(sign_only(date_cmp(small2, medium)), -1);
	t.is(sign_only(date_cmp(big1, medium)),    1);
	t.is(sign_only(date_cmp(big2, medium)),    1);
});
