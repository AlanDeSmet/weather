#! /usr/bin/node

'use strict';

/* START import of tinyduration, Copyright 2020 MelleB */
/*
Copyright 2020 MelleB

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */
var units = [
    { unit: 'years', symbol: 'Y' },
    { unit: 'months', symbol: 'M' },
    { unit: 'weeks', symbol: 'W' },
    { unit: 'days', symbol: 'D' },
    { unit: 'hours', symbol: 'H' },
    { unit: 'minutes', symbol: 'M' },
    { unit: 'seconds', symbol: 'S' },
];

var r = function (name, unit) { return "((?<".concat(name, ">-?\\d*[\\.,]?\\d+)").concat(unit, ")?"); };
var durationRegex = new RegExp([
    '(?<negative>-)?P',
    r('years', 'Y'),
    r('months', 'M'),
    r('weeks', 'W'),
    r('days', 'D'),
    '(T',
    r('hours', 'H'),
    r('minutes', 'M'),
    r('seconds', 'S'),
    ')?', // end optional time
].join(''));

function parseNum(s) {
    if (s === '' || s === undefined || s === null) {
        return undefined;
    }
    return parseFloat(s.replace(',', '.'));
}

var InvalidDurationError = new Error('Invalid duration');

function parse_duration(durationStr) {
    var match = durationRegex.exec(durationStr);
    if (!match || !match.groups) {
        throw InvalidDurationError;
    }
    var empty = true;
    var values = {};
    for (var _i = 0, units_1 = units; _i < units_1.length; _i++) {
        var unit = units_1[_i].unit;
        if (match.groups[unit]) {
            empty = false;
            values[unit] = parseNum(match.groups[unit]);
        }
    }
    if (empty) {
        throw InvalidDurationError;
    }
    var duration = values;
    if (match.groups.negative) {
        duration.negative = true;
    }
	//console.log(duration);
    return duration;
}
/* end import of tinyduration, Copyright 2020 MelleB */


// This is only accurate if years, months, weeks, and days
// are all zero (or unspecified).
export function duration_to_approximate_s(str) {
	console.assert(str.length >= 1, `duration_to_approximate_s(${str}): input should be at least 1 character long, but is ${str.length}`);
	console.assert(str[0] == 'P', `duration_to_approximate_s(${str}): input should start with 'P', but is ${str[0]}`);
	var durobj = parse_duration(str);
	var years   =  durobj['years'] || 0;
	var months  =  durobj['months'] || 0;
	var days    = (durobj['days'] || 0) + (durobj['weeks'] || 0)*7;
	var seconds = 
		(durobj['hours']   || 0)  * 60*60 +
		(durobj['minutes'] || 0)  * 60 +
		(durobj['seconds'] || 0);
	var total_seconds =
		years  *365.25  *24*60*60+
		months * 30.4375*24*60*60+
		days            *24*60*60+
		seconds ;
	return total_seconds;
}

function assert_length(func, instr, target_len) {
	console.assert(instr.length == target_len, `${func}(${instr}): input should be ${target_len} characters long, but is ${instr.length}`);
}

export function baredatestr_to_date(timestr) {
	//          1        
	//0123456789012345678
	//2022-09-12T19:00:00
	assert_length('baredatestr_to_date', timestr, 19);
	return new Date(timestr);
}

export function tz_to_s(timestr) {
	// 012345
	// -00:00
	assert_length('tz_to_s', timestr, 6);
	let offset_hour_str = timestr.substring(0,3);
	let offset_hour = parseInt(offset_hour_str);
	let offset_minute_str = timestr.substring(4);
	let offset_minute = parseInt(offset_minute_str);
	offset_minute += offset_hour*60;
	let offset_s = offset_minute*60;
	//console.log("converted",timestr,"to",offset_s);
	return offset_s;
}

export function date_plus_s(date,s) {
	var d = new Date(date);
	d.setSeconds(d.getSeconds()+s);
	return d;
}

export function date_cmp(date_a, date_b) {
	return date_a - date_b;
}

export function datestr_to_date(timestr) {
	//           1         2
	// 0123456789012345678901234
	// 2022-09-12T19:00:00-05:00
	console.assert((timestr.length == 25) || (timestr.length == 19), `datestr_to_date(${timestr}): input should be 19 or 25 characters long, but is ${timestr.length}`);
	var date = baredatestr_to_date(timestr.substring(0,19));
	var tz = timestr.substring(19,25);
	if(tz.length == 6) {
		date = date_plus_s(date, -tz_to_s(timestr.substring(19,25)));
	}
	return date;
}

export function datestr_range_to_dates(timestr) {
	console.assert((timestr.length >= 21), `datestr_range_to_dates(${timestr}): input should be at least 21 character long, but is ${timestr.length}`);
	var parts = timestr.split("/");
	var start = datestr_to_date(parts[0]);
	var duration = duration_to_approximate_s(parts[1]);
	var end = date_plus_s(start, duration);
	return [start, end];
}

export class DateDict {
	constructor() {
		this.dict = {};
	}
	set(key, val) {
		this.dict[key.toISOString()] = [key, val];
	}
	get(key, defaultval=undefined) {
		var skey = key.toISOString();
		if(skey in this.dict) {
			return this.dict[key.toISOString()][1];
		}
		return defaultval;
	}
	has(key) {
		var skey = key.toISOString();
		return skey in this.dict;
	}
	keys() {
		var values = Object.values(this.dict);
		var out = [];
		for(const value of values) {
			out.push(value[0]);
		}
		return out;
	}
	map(fn) {
		var out = new DateDict();

		var values = Object.values(this.dict);
		for(const value of values) {
			out.set(value[0], fn(value[1]));
		}
		return out;
	}
}
