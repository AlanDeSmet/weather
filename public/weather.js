'use strict';


import {getSunTimes} from "./suncalc.js";
import {DateDict, date_cmp, date_plus_s, tz_to_s, datestr_to_date, datestr_range_to_dates} from "./iso8601.js";

var DEFAULT_LATITUDE  =  43.0707;
var DEFAULT_LONGITUDE = -89.4068;
// Southern Florida
//var DEFAULT_LATITUDE  =  25.8880;
//var DEFAULT_LONGITUDE = -80.3068;

const MINUTE_S = 60;
const HOUR_S = 60*MINUTE_S;

const EMOJI = "\uFE0F";
const FIGURESPACE = " ";

const MAX_DOWNLOAD_DELAY = 60.0;
const STARTING_DOWNLOAD_DELAY = 0.5;
const DOWNLOAD_DELAY_MULTIPLIER = 2.0;



function sleep(time) {
	return new Promise((resolve) => setTimeout(resolve, time*1000));
}


function remove_all_children(node) {
	while (node.firstChild) {
		node.removeChild(node.firstChild)
	}
}

function replace_children_with_text(node, text) {
	remove_all_children(node);
	node.append(document.createTextNode(text));
}

async function fetch_wrap(url) {
	// Artificial delays
	if(false) {
		const wait = Math.random()*2
		console.log(`WARNING: artificial wait in place, waiting ${wait} esconds}`);
		await sleep(Math.random()*2);
	}

	// Artificial errors
	if(false) {
		if(Math.floor(Math.random()*2) == 0) {
			return {
				"ok": false,
				"status": 500,
				"statusText": "Internal Server Error",
			};
		}
	}
	
	return await fetch(url)
}

async function get_json(url, purpose) {
	const output_div = document.getElementById('status');
	var status_div = simpletag('div', "", output_div);
	simpletag('div', "Downloading "+purpose, status_div);
	console.log("Requesting", url, "for", purpose);
	var response = {"ok": false};
	var naptime = STARTING_DOWNLOAD_DELAY;

	while(true) {
		response = await fetch_wrap(url);
		if(response.ok) { break; }
		if(response.status == 404) {
			const msg = `Fatal Error: No data is available for this location. Data is only available for United States locations. (${response.status} ${response.statusText})`;
			simpletag('div', msg, status_div);
			throw new Error(msg);
		}
		if(response.status < 500 || response.status > 599) {
			const msg = `Fatal Error: fetch(${url}) returned a non-2XX result, and because it's not 5XX, it will not be retried: ${response.status} ${response.statusText}`;
			simpletag('div', msg, status_div);
			throw new Error(msg);
		}
		if(naptime > MAX_DOWNLOAD_DELAY) {
			simpletag('div', `Fatal Error ${purpose}: Ran out of time (maximum timeout of ${MAX_DOWNLOAD_DELAY} seconds)`, status_div);
			throw new Error(`FATAL: fetch(${url}) returned a non-2XX result: ${response.status} ${response.statusText}`);
		}
		simpletag('div', `Error ${purpose}: ${response.status} ${response.statusText}. Retrying after ${naptime} seconds.`, status_div);
		await sleep(naptime);
		naptime *= DOWNLOAD_DELAY_MULTIPLIER;
	}
	remove_all_children(status_div)
	simpletag('div', "Successful "+purpose, status_div);
	return response.json()
}

async function get_json_or_describe_error(url, node, purpose) {
	try {
		var result = await get_json(url, purpose);
		console.log(`Retrieved ${url} for ${purpose}`);
		return result;
	} catch (e) {
		console.log(`Failed to retrieve ${url} for ${purpose}: ${e}`);
		replace_children_with_text(node, `Problem getting ${purpose}: ${e}`);
		throw e;
	}
}

/*
async function get_hourly_forecasts(point_info) {
	var result = await get_json(point_info.properties.forecastGridData);
	return result;
}
*/

function simpletag(tag, text, root = null, addclass=null) {
	var elemtag = document.createElement(tag);
	elemtag.append(document.createTextNode(text));
	if (root !== null) {
		root.append(elemtag)
	}
	if (addclass != null) {
		elemtag.classList.add(addclass);
	}
	return elemtag;
}

function set_title(point_info) {
	const output_div = document.getElementById('forecasttitle');
	var coords = point_info.geometry.coordinates;
	var loc_props = point_info.properties.relativeLocation.properties;
	var outloc = `Weather Forecast for ${loc_props.city}, ${loc_props.state} (${coords[1]}, ${coords[0]})`;
	replace_children_with_text(output_div, outloc);
	document.title = outloc;
}

function render_daily_forecasts(data, output_div) {
	remove_all_children(output_div)
	var daily_forecasts = data.properties;
	var time_to_dd = [];
	var section = simpletag("div", "", output_div, "daily");
	for(const day of daily_forecasts.periods) {
		var name = day.name.replace(" ", "\xA0") // A0 == &nbsp;
		var halfday_forecast_block = simpletag("div", "", section);
		simpletag("h2", name, halfday_forecast_block);
		var half_day_div = simpletag("div", day.detailedForecast, halfday_forecast_block, "halfday")

		var hourly_div = simpletag("div", "", section, "hourly")

		/*var clear_div = simpletag("div", "", halfday)
		clear_div.classList.add("clear");*/

		time_to_dd.push({
			startTime: datestr_to_date(day.startTime),
			endTime: datestr_to_date(day.endTime),
			div: hourly_div
		})
	}
	return time_to_dd;
}

function render_point_info(point_info) {
	console.log("point info succeeded", point_info);
	set_title(point_info);
}

function collect_tz_offsets(daily_data) {
	var offsets = [];
	for(const period of daily_data.properties.periods) {
		//          1         2
		//0123456789012345678901234
		//2022-09-12T19:00:00-05:00
		let utc_starttime = datestr_to_date(period.startTime);
		let offset_s = tz_to_s(period.startTime.substring(19,25));
		offsets.push([utc_starttime, offset_s]);
	}
	offsets.sort((a,b)=> a[0] - b[0])
	return offsets;
}

function get_valid_times(validtimes) {
	var nowdt = new Date()
	nowdt.setMinutes(0);
	nowdt.setSeconds(0);
	nowdt.setMilliseconds(0);
	var now = date_plus_s(nowdt, nowdt.getTimezoneOffset()*MINUTE_S);
	var [starttime, endtime] = datestr_range_to_dates(validtimes)
	var results = [];
	for(var time = starttime; date_cmp(time, endtime) < 0; time = date_plus_s(time, HOUR_S)) {
		if(date_cmp(time, now) >= 0) {
			results.push(time);
		} else {
			console.log(`Discarding ${time} as in the past`);
		}
	}
	return results;
}

function key_by_time(values) {
	const HOUR = 60*60;
	var out = new DateDict();
	for(const value of values) {
		var [msepoch, last_time] = datestr_range_to_dates(value.validTime);
		for( ; date_cmp(msepoch,last_time) < 0; msepoch = date_plus_s(msepoch, HOUR)) {
			out.set(msepoch, value.value);
		}
	}
	//console.log(out);
	return out;
}

class Values {
	constructor(name, upstream) {
		this.name = name
		this.unit_uom = upstream.uom;
		this._values = key_by_time(upstream.values);
	}

	remapped_values(fn) {
		return this._values.map(fn);
	}

	values(metric=true) {
		if(metric) {
			return this._values
		}
		if(this.unit_uom == 'wmoUnit:degC') {
			return this.remapped_values(CtoF);
		}
		if(this.unit_uom == 'wmoUnit:mm') {
			return this.remapped_values(mmtoin);
		}
		return this._values;
	}


	fmtnum(val, metric=true) {
		var digits = {
			'nwsUnit:s': 0,
			'wmoUnit:degC': 1,
			'wmoUnit:degree_(angle)': 0,
			'wmoUnit:km_h-1': 0,
			'wmoUnit:m': 0,
			'wmoUnit:mm': 0,
			'wmoUnit:percent': 0,
		};
		var max_digit = {
			'nwsUnit:s': 1,
			'wmoUnit:degC': 2,
			'wmoUnit:degree_(angle)': 3,
			'wmoUnit:km_h-1': 3,
			'wmoUnit:m': 6,
			'wmoUnit:mm': 3,
			'wmoUnit:percent': 3,
		}
		if(!metric) {
			digits = {
				'nwsUnit:s': 0,
				'wmoUnit:degC': 0,
				'wmoUnit:degree_(angle)': 0,
				'wmoUnit:km_h-1': 0,
				'wmoUnit:m': 0,
				'wmoUnit:mm': 1,
				'wmoUnit:percent': 0,
			};
			max_digit['wmoUnit:degC'] = 3
		}
		var d = digits[this.unit_uom] || 0;
		var ret = val.toFixed(d);

		var decimalpoint = ret.indexOf(".")
		if(decimalpoint == -1) {decimalpoint = ret.length; }
		var intpart = ret.substring(0,decimalpoint)
		var needed = max_digit[this.unit_uom] - intpart.length
		if(needed > 0) {
			var padding = FIGURESPACE.repeat(needed)
			ret = padding + ret
		}
		return ret;
	}

	unit(metric=true) {
		var tohuman = {
			'nwsUnit:s': 's',
			'wmoUnit:degC': '°C',
			'wmoUnit:degree_(angle)': '°',
			'wmoUnit:km_h-1': 'km/h',
			'wmoUnit:m': 'm',
			'wmoUnit:mm': 'mm', 
			'wmoUnit:percent': '%',
		}
		if(!metric) {
			tohuman = {
				'nwsUnit:s': 's',
				'wmoUnit:degC': '°F',
				'wmoUnit:degree_(angle)': '°',
				'wmoUnit:km_h-1': 'km/h TODO',
				'wmoUnit:m': 'm TODO',
				'wmoUnit:mm': '"', 
				'wmoUnit:percent': '%',
			}
		}
		return tohuman[this.unit_uom] || this.unit_uom.split(":")[1] || this.unit_uom
	}
}


function get_time_offset(offsets, date) {
	for(const offset of offsets) {
		if(date_cmp(date, offset[0])>0) {
			return offset[1];
		}
	}
	console.log(`ERROR: Problem finding offset for ${date}`, offsets);
	return offsets[0][1];
}

function near_date(date) {
	return date.toLocaleDateString('default')
	//return `${date.getFullYear()}-${date.getMonth()}-${date.getDate()} ${hour}${ampm}`
}

function nice_time(date, output_div) {
	var hour = date.getHours();
	var ampm = "am";
	if(hour == 0) {
		hour = 12;
	}
	if(hour >= 12) {
		if(hour > 12) { hour -= 12 }
		ampm = "pm";
	}
	hour = ""+hour
	if(hour.length == 1) {
		hour = FIGURESPACE+hour;
	}
	output_div.append(document.createTextNode(`${hour}`));
	var ampm_span = simpletag("span", ampm, output_div, "ampm");
}

function mmtoin(mm) {
	return mm/25.4;
}

function CtoF(c) {
	return c*9/5+32;
}

function icons_for_weather(weather, isday) {
	/*
	COVERAGE: NULL OR  AREAS, BRIEF, CHANCE, DEFINITE, FEW, FREQUENT,
	INTERMITTENT, ISOLATED, LIKELY, NUMEROUS, OCCASIONAL, PATCHY, PERIODS,
	SCATTERED, SLIGHT_CHANCE, WIDESPREAD

	WEATHER: NULL OR  BLOWING_DUST, BLOWING_SAND, BLOWING_snow, drizzle, fog,
	FREEZING_fog, FREEZING_drizzle, FREEZING_rain, FREEZING_SPRAY, FROST, hail,
	HAZE, ICE_CRYSTALS, ICE_FOG, rain, rain_SHOWERS, SLEET, SMOKE, snow,
	snow_SHOWERS, THUNDERSTORMS, VOLCANIC_ASH, WATER_SPOUTS

	INTENSITY: NULL OR  VERY_LIGHT, LIGHT, MODERATE, HEAVY

	VISIBILITY: $REF: '#/COMPONENTS/SCHEMAS/QUANTITATIVEVALUE'

	ATTRIBUTES: DAMAGING_WIND, dry_THUNDERSTORMS, FLOODING, GUSTY_WIND,
	HEAVY_RAIN, LARGE_HAIL, SMALL_HAIL, TORNADOES
	*/
	var icons = ""
	var val = weather;
	var intensity = val.intensity || "";
	intensity = intensity.replace('_', ' ');
	if(intensity.length > 0) { intensity += " "; }
	if(val.weather === null) { 
		if(isday) { return "☀️"+EMOJI; }
		return '🌌'+EMOJI;
		//return "🌒"+EMOJI; 
	}
	if(val.weather.includes('rain') || val.weather == "thunderstorms") {
		icons += '💧'+EMOJI;
		if(val.attributes.includes('heavy_rain') || val.intensity == 'heavy') {
			icons += '💧'+EMOJI;
		}
		//icons += '🌧'+EMOJI;
	}
	if(val.weather.includes('drizzle')) {
		icons += '💧'+EMOJI;
		icons += '🌧'+EMOJI;
	}
	if(val.weather == "thunderstorms") {
		icons += '⚡'+EMOJI;
		/*
		if(val.attributes === null || !val.attributes.includes('dry_thunderstorms')) {
			icons += '⛈'+EMOJI;
		} else {
			icons += '🌩'+EMOJI;
		}
		*/
	}
	if(val.weather.includes('snow')) {
		//icons += '🌨'+EMOJI;
		icons += '❄️'+EMOJI
		if(val.intensity == 'heavy') {
			icons += '❄️'+EMOJI
		}
	}
	if(val.weather == 'hail' || val.attributes.includes('hail')) {
		icons += '🧊'+EMOJI;
	}
	if(val.weather.includes('fog')) {
		icons += '🌫️'+EMOJI;
	}
	if(val.attributes.includes('tornadoes')) {
		icons += '🌪'+EMOJI;
	}

	return icons;
}

function descriptions_for_weather(weather) {
	var val = weather.value;
	var coverage = weather.coverage || "";
	var intensity = weather.intensity || "";
	var weather = weather.weather || "";
	//var visibility = weather.visibility || {};
	var attributes = weather.attributes || [];

	var result = [];
	if(coverage.length > 0) { result.push(coverage); }
	if(intensity.length > 0) { result.push(intensity); }
	if(weather.length > 0) { result.push(weather); }
	if(attributes.length > 0) {
		result.push("(" + attributes.join(", ") + ")");
	}

	result = result.join(" ");
	result = result.replace(/_/g, ' ');
	return result;
}

function get_default(obj, key, defaultval=null) {
	console.log(obj);
	console.log("key", key);
	key = key.toISOString();
	console.log("key", key);
	if(key in obj) {
		console.log("returning", obj[key]);
		return obj[key];
	}
	console.log("key not in obj!?!?");
	return defaultval;
}

function add_number_cell(numberstr, unit, row, prefix="", suffix="") {
	if(numberstr === null) { numberstr = "—"; }
	var t = simpletag("td", prefix+numberstr, row, "n");
	if(unit.length > 0) {
		simpletag('span',unit, t);
	}
	if(suffix.length > 0) {
		t.append(document.createTextNode(suffix));
	}
}

function add_raw_var_cell(variable, value, row, metric) {
	var valstr = null;
	if(value !== null) {
		valstr = ""+variable.fmtnum(value, metric);
	}
	var unit = variable.unit(metric);
	add_number_cell(valstr, unit, row)
}

function add_var_cell(variable, time, row, metric) {
	var values = variable.values(metric);
	var value = null
	if(values.has(time)) {
		value = values.get(time)
	}
	add_raw_var_cell(variable, value, row, metric);
}

function render_half_day_of_hourly_forecasts(variables, hourly_data, tz_offsets, all_times, output_div, suntimes) {
	var tbl = document.createElement("table");
	tbl.classList.add("hourly")

	var t;

	/*
	var row = document.createElement("tr");
	simpletag("th", "Time", row);
	simpletag("th", "🌡"+EMOJI, row);
	simpletag("th", "", row);
	var t = simpletag("th", '🌧'+EMOJI+ '🌨'+EMOJI, row);
	t.setAttribute("colspan", "2");
	//simpletag("th", "", row);
	tbl.append(row);
	*/

	var metric = false;

	var fakerow = document.createElement("tr");
	fakerow.classList.add("fakerow");
	fakerow.style="visibility: collapse";
	t = simpletag("td", "", fakerow, "n")
	// TODO: check if we're correcting for local time, in which case this doesn't do what I think it does in some time zones
	nice_time(new Date("2022-12-29T23:59:59Z"), t);
	add_raw_var_cell(variables["apparentTemperature"], 999, fakerow, metric);

	var v_tmp = variables["temperature"];
	var valstr = ""+v_tmp.fmtnum(999, metric);
	var unit = v_tmp.unit(metric);
	add_number_cell(valstr, unit, fakerow, "(",")");

	add_raw_var_cell(variables["probabilityOfPrecipitation"], 100, fakerow, metric);
	add_raw_var_cell(variables["quantitativePrecipitation"], 99, fakerow, metric);

	tbl.append(fakerow);

	for(const thistime of all_times) {
		var row = document.createElement("tr");

		var tz_offset = get_time_offset(tz_offsets, thistime)
		var this_date = date_plus_s(thistime, tz_offset);

		var isday = false;
		if(suntimes) {
			var cls = "night";
			var sunrise = suntimes.sunrise(this_date);
			var sunset = suntimes.sunset(this_date);
			var after_sunrise = date_cmp(this_date, suntimes.sunrise(this_date)) > 0;
			var before_sunset = date_cmp(this_date, suntimes.sunset(this_date)) < 0;
			if( after_sunrise && before_sunset) {
				cls = "day";
				isday = true;
			}
		}
		row.classList.add(cls);

		cls = "even";
		if(this_date.getHours()%2) {
			cls = "odd";
		}
		row.classList.add(cls);

		t = simpletag("td", "", row, "n")
		nice_time(this_date, t);

		add_var_cell(variables["apparentTemperature"], thistime, row, metric);
		var aptemp = variables["apparentTemperature"].values(metric).get(thistime, "UNKNOWN_TEMP");
		var nomtemp = v_tmp.values(metric).get(thistime, "UNKNOWN_TEMP2");
		if(aptemp != nomtemp) {
			var valstr = ""+v_tmp.fmtnum(nomtemp, metric);
			var unit = v_tmp.unit(metric);
			add_number_cell(valstr, unit, row, "(",")");
		} else {
			add_number_cell("", "", row);
		}
		add_var_cell(variables["probabilityOfPrecipitation"], thistime, row, metric);
		add_var_cell(variables["quantitativePrecipitation"], thistime, row, metric);
		var weather_list = variables['weather'].values().get(thistime);
		var icons = ""
		var descriptions = ""
		for(const weather of weather_list) {
			icons += icons_for_weather(weather, isday);
			descriptions += descriptions_for_weather(weather);
		}
		var td = simpletag("td", "", row);
		if(descriptions.length) {
			var abbr = simpletag("abbr", icons, td);
			abbr.setAttribute("title", descriptions);
		} else {
			td.append(document.createTextNode(icons));
		}
		tbl.append(row);
	}

	remove_all_children(output_div)
	output_div.append(tbl);
}

function noonify(date) {
	var ret = new Date(date)
	ret.setHours(12);
	ret.setMinutes(0);
	ret.setSeconds(0);
	ret.setMilliseconds(0);
	return ret;
	//return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 12, 0, 0);
}

function datestr(date) {
	return `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
}

class SunriseSunset {
	constructor(all_times, latitude, longitude) {
		self.times = {}
		var last_day = -1;
		for(const time of all_times) {
			if(time.getDate() == last_day) { continue; }
			last_day = time.getDate();
			var noon = noonify(time);
			var times = getSunTimes(noon, latitude, longitude);
			
			self.times[datestr(noon)] = times;
		}
	}
	sunrise(date) {
		var lookup = datestr(date);
		if(lookup in self.times) {
			return self.times[lookup].sunrise;
		}
		return null;
	}
	sunset(date) {
		var lookup = datestr(date);
		if(lookup in self.times) {
			return self.times[lookup].sunset;
		}
		return null;
	}
}

function render_hourly_forecast(hourly_data, daily_data, daily_render, output_div, latitude, longitude) {
	var to_collect = {
		"apparentTemperature": "Temp.",
		"temperature": "Temp (Nominal)",
		"probabilityOfPrecipitation": "Chance Precip.", 
        "quantitativePrecipitation": "Precip.",
		"weather": "Summary",
		//"skycover",
		//"hazards",
		//"snowfallAmount",
		//"lightningActivityLevel",
		//"probabilityOfThunder"
		//bunch of winds by speed
	};
	console.log("daily render:",daily_render);

	var tz_offsets = collect_tz_offsets(daily_data);

	var forecast = hourly_data.properties;

	var all_times = get_valid_times(hourly_data.properties.validTimes);

	var suntimes = new SunriseSunset(all_times, latitude, longitude);

	var variables =  { };
	for(const [varname, outname] of Object.entries(to_collect)) {
		variables[varname] = new Values(outname, forecast[varname]);
	}


	for(const daily of daily_render) {
		var these_times = [];
		for(const time of all_times) {
			if( (date_cmp(time, daily.startTime) >= 0) &&
			    (date_cmp(time, daily.endTime)   <  0)) {
				these_times.push(time);
			}
		}

		render_half_day_of_hourly_forecasts(variables, hourly_data, tz_offsets, these_times, daily.div, suntimes);
	}



}

/*
get_point
   |-----------> render_point
   |-----------> get_daily
   |                  `----------> render_daily
   |                                     `------,----> render_hourly
   |                  ,-------------------------'    
   `-----------> get_hourly

p_gpoint = get_point_info
p_gpoint.then(render_point)
p_gpoint.then(
		p_ghourly = get_hourly
		get_daily.then(
			p_rdaily =render_daily
			all(p_rdaily, pg_hourly).then(render_hourly)
		)
		)

 */

export async function load_weather() {
	var params = new URLSearchParams(window.location.search);

	var latitude = parseFloat(params.get('latitude')) || DEFAULT_LATITUDE;
	var longitude = parseFloat(params.get('longitude')) || DEFAULT_LONGITUDE;

	const status_div = document.getElementById('status');
	if(longitude > 360 || longitude < -180) {
		const msg = `Fatal Error: Invalid longitude. It must be between -180 and 360.`;
		simpletag('div', msg, status_div);
		return
	}
	if(latitude > 90 || latitude < -90) {
		const msg = `Fatal Error: Invalid latitude. It must be between -90 and 90.`;
		simpletag('div', msg, status_div);
		return
	}
	if(longitude > 180) {
		longitude -= 360;
	}



	var title_node = document.getElementById("forecasttitle");
	var daily_forecasts_node = document.getElementById("dailyforecasts");
	var hourly_forecasts_node = document.getElementById("hourlyforecasts");

	var point_info_url = `https://api.weather.gov/points/${latitude},${longitude}`;

	var prms_pt_info = get_json_or_describe_error(point_info_url, title_node, "acquire general location information");
	prms_pt_info.then(render_point_info);
	prms_pt_info.then(info => {
		let prms_hourly = get_json_or_describe_error(info.properties.forecastGridData, hourly_forecasts_node, "acquire hourly forecasts");

		let prms_daily = get_json_or_describe_error(info.properties.forecast, daily_forecasts_node, "acquire daily forecasts");
		prms_daily.then( daily_data=>{
			let prms_rdaily = render_daily_forecasts(daily_data,daily_forecasts_node)
			Promise.all([prms_hourly, prms_rdaily])
				.then( data=>render_hourly_forecast(data[0], daily_data, data[1], hourly_forecasts_node, latitude, longitude));
		});
	});
	console.log("version 22");

}


